// Import Render Passes From Folder

#target photoshop

app.bringToFront();

/**
 * Displays error in form of pop-up dialog
 * @param  {Error} err Error structure passed from Photoshop API
 */
function showError(err) {
	alert(err + ': on line ' + err.line, 'Script Error', true);
}

/**
 * Creates a folder dialog and then lists out paths of supported files
 * @return {Array}      List of supported files in the folder
 */
function getRenderPasses() {

    var importFolder = Folder.selectDialog('Select the folder to be imported:', Folder("~"))
	
    // Check if chosen folder is valid
    if (!importFolder) {
        // User cancelled the dialog
		return null;
	} else if (!importFolder.exists) {
		alert('Folder not found.', 'Script Stopped', true);
		return null;
	}

    var filesForImport = new Array();
    // Get filepaths from the import folder
    var files = importFolder.getFiles();
	for (var i = 0; i < files.length; i++) {
		if (files[i] instanceof File && files[i].name.match(/\.(?:png|jpg|bmp|tif|gif)$/i)) {
			filesForImport.push(files[i]);
		}
	}
	return filesForImport;
}

/**
 * Imports a given file to Photoshop, renames it and moves it to target document or create new document from imported file
 * @param  {String}     filePath            File path of imported file
 * @param  {Document}   targetDocument      Target document for imported layer
 * @return {Document}                       Returns target document
 */
function importFileAsLayer(filePath, targetDocument) {    
    // Open file in a new document
    var importDocument = open(filePath);
    
    // Extract name without file extension and rename imported layer
    importDocument.activeLayer.name = importDocument.name.replace(/(?:\.[^.]*$|$)/, '');
    
    if (targetDocument == null) {
        return importDocument;
    } else {
        // Duplicate layer to current document
        importDocument.activeLayer.duplicate(targetDocument, ElementPlacement.PLACEATBEGINNING);
        // close imported document
        importDocument.close(SaveOptions.DONOTSAVECHANGES);
        return targetDocument;
    }
}

/**
 * Imports all render passes from a folder to current document or new one
 */
function importRenderPasses() {
    var currentDoc = null
    if (app.documents.length != 0) {
        currentDoc = app.activeDocument;
    }

    var filesForImport = getRenderPasses();
    if (filesForImport == null) {
        alert("Could not open the folder.", 'Folder unrecognised', false);
        return;
    }

    if (filesForImport.length == 0) {
        alert("The selected folder doesn't contain any recognized images.", 'No Files Found', false);
        return;
    }

    var new_file = false;
    if (currentDoc == null) {
        new_file = true;
    }

    for (var i = 0; i < filesForImport.length; i++) {
		currentDoc = importFileAsLayer(filesForImport[i], currentDoc);
	}
    
    if (new_file) {
        currentDoc.save();
    }
}

try {
    importRenderPasses();
}
catch(e) {
    if (e.number != 8007) { // Avoid dialog canceling from user
        showError(e);
    }
}
