bl_info = {
    "name": "Auto Render Pass Saver",
    "description": "Exports render passes and crytomatte masks by collection in given location.",
    "blender": (3, 1, 0),
    "category": "Compositing",
    "author": "Michal Rudolf",
    "version": (1, 0),
    "location": "Compositor Tab > Sidebar > Auto Render Pass Saver",
    "support": "COMMUNITY",
}

import bpy
import subprocess
import os

class RenderPassSaverProperties(bpy.types.PropertyGroup):
    """Class containing user preference properties"""
    export_folder_path: bpy.props.StringProperty(
        name = "Path",
        description = "Path to the folder, where you want to export all render passes",
        default = "/tmp/output",
        subtype = 'DIR_PATH'
        )

    auto_render: bpy.props.BoolProperty(
        name = "Auto Render",
        description = "Rendering will automatically start after clicking on 'Render Pass Saver' Buttons",
        default = True
        )

    open_export_folder: bpy.props.BoolProperty(
        name = "Open destination folder",
        description = "After rendering is complete, open the export folder, where the output is going to be saved. This takes effect only when 'Auto Render' is enabled",
        default = False
        )

    output_prefix: bpy.props.StringProperty(
        name = "Prefix",
        description = "Prefix name, that will be added before pass name (e. g. prefix_passname.png). If this is left blank, no prefix will be applied to pass names",
        default = "",
        subtype = 'FILE_NAME'
        )
    mask_by_collection: bpy.props.BoolProperty(
        name = "Mask out by collection",
        description = "Cryptomatte object mask, common for objects in the same collection, will be applied to rendered image (for each collection). Each masked image will be named after the collection and exported separately. Root collection and collections containing only Lights and Cameras will be skipped",
        default = True,
        )
    denoise_all_passes: bpy.props.BoolProperty(
        name = "Denoise all passes",
        description = "RECOMMENDED: Denoise all available passes with Denoise data",
        default = True,
        )
    other_layers_combined: bpy.props.BoolProperty(
        name = "Render combined from all layers",
        description = "Render combined pass from all other enabled (Use for Rendering)",
        default = False,
        )

# -------------------------------------------------------------

def ClearAllNodes():
    """Clears all composition nodes from Compositor Node Tree"""
    node_tree = bpy.context.scene.node_tree
    for node in node_tree.nodes:
        node_tree.nodes.remove(node)

def ComposeFullName(properties, short_name):
    """Composes an export files name with prefix, render pass name and # at the end"""
    full_name = properties.output_prefix
    if len(full_name):
        full_name += '_'
    full_name += short_name + '#' # Adding postfix number of the frame can't be avoided (this is reducing it to 1 digit)
    return full_name

class RenderPassButton(bpy.types.Operator):
    """Generates necessary compositor nodes for rendering and export of selected passes"""
    bl_idname = "node.render_pass"
    bl_label = " Render Pass Saver"
    bl_options = {'REGISTER', 'UNDO'}    

    render_stop = False
    rendering = False
    render_done = False
    ui_timer = None

    # ------------- Callbacks ---------------
    def PreRender(self, scene, context=None):
        """Modal callback to call before rendering"""
        self.rendering = True
        
    def PostRender(self, scene, context=None):
        """Modal callback to call after rendering"""
        self.rendering = False
        self.render_done = True

    def CancelledRender(self, scene, context=None):
        """Modal callback to call, when rendering is cancelled"""
        self.render_stop = True

    # ---------------- Open Folder --------------------
    def OpenExportFolder(self):
        """Opens export folder using shell therminal"""
        properties = bpy.context.scene.RenderPassSaverProperties
        subprocess.call("explorer " + properties.export_folder_path, shell = True)

    # ---------------- Cryptomatte Masking -----------------------
    def IsCryptomatteObjectEnabled(self, source_node):
        """Checks if cryptomate object pass option in render properties is enabled"""
        for out in source_node.outputs:
            if 'CryptoObject' in out.identifier:
                return True
        return False

    def CreateAndLinkCryptomatte(self, collection_name, layer_node, output_node, position, objects):
        """Creates and link properly cryptomatte-V2 node's input and output"""
        node = bpy.context.scene.node_tree.nodes.new("CompositorNodeCryptomatteV2")
        node.location = position
        node.hide = True
        for obj in objects:
            if len(node.matte_id) == 0:
                node.matte_id += (obj.name)
            else:
                node.matte_id += "," + (obj.name)
                
        properties = bpy.context.scene.RenderPassSaverProperties
        new_slot = output_node.file_slots.new(ComposeFullName(properties, collection_name))
        
        bpy.context.scene.node_tree.links.new(layer_node.outputs[0], node.inputs[0])
        bpy.context.scene.node_tree.links.new(node.outputs[0], new_slot)
    
    def ApplyCryptomatteObjectMask(self, layers_node, output_node):
        """Loops through collections and apply masks"""
        idx = 0
        for collection in bpy.data.collections:
            # Check if collection have any visible object (All types are considered visible apart from 'LIGHT' and 'CAMERA')
            # It won't mask out collections that contain only lights or cameras
            if len(collection.all_objects) == 0:
                continue
            have_visible_objects = False
            for o in collection.all_objects:
                if 'CAMERA' in o.type or 'LIGHT' in o.type:
                    continue
                have_visible_objects = True
            if not have_visible_objects:
                continue
            # Create cryptomatte node and set up IDs of object from the collection
            self.CreateAndLinkCryptomatte(collection.name, layers_node, output_node, (100, -100 - idx * 35), collection.all_objects)
            idx += 1
    
    # ------------------- Normalizing ----------------------    
    def CreateNormalized(self, position):
        """Creates Normalize node on given position"""
        normalize_node = bpy.context.scene.node_tree.nodes.new("CompositorNodeNormalize")
        normalize_node.hide = True
        normalize_node.location = position
        return normalize_node
    
    # ------------------ Denoising ------------------------
    def CreateAndLinkDenoise(self, img_socket, normal_socket, albedo_socket, output_socket, position):
        """Creates denoise node and link it with Denoise data and output file socket"""
        denoise_node = bpy.context.scene.node_tree.nodes.new("CompositorNodeDenoise")
        denoise_node.hide = True
        denoise_node.location = position
        
        bpy.context.scene.node_tree.links.new(img_socket, denoise_node.inputs[0])
        bpy.context.scene.node_tree.links.new(normal_socket, denoise_node.inputs[1])
        bpy.context.scene.node_tree.links.new(albedo_socket, denoise_node.inputs[2])
        bpy.context.scene.node_tree.links.new(denoise_node.outputs[0], output_socket)

    def IsDenoiseEnabled(self, render_node):
        """Checks if user specified Denoise option"""
        for out in render_node.outputs:
            if "Denoising" in out.identifier:
                return True
        return False

    def FindAlbedoDenoiseSocket(self, render_node):
        """Checks if Denoise Albedo pass is enabled"""
        for out in render_node.outputs:
            if "Denoising Albedo" == out.identifier:
                return out
        return None
    
    def FindNormalDenoiseSocket(self, render_node):
        """Checks if Denoise Normal pass is enabled"""
        for out in render_node.outputs:
            if "Denoising Normal" == out.identifier:
                return out
        return None        

    # ------------------ Render Node ------------------------
    def CreateRenderLayers(self, position, render_layer):
        """Creates Render Layers node on given position and sets up render layer"""
        node = bpy.context.scene.node_tree.nodes.new('CompositorNodeRLayers')
        node.location = position
        node.use_custom_color = True
        node.color = (0.600, 0.200, 0.200)
        node.layer = render_layer
        return node        

    def LinkRenderPasses(self, source_node, output_node, albedo_socket, normal_socket):
        """Creates correct socket in File Output node and link it with render pass"""
        idx = 0
        properties = bpy.context.scene.RenderPassSaverProperties
        for out in source_node.outputs:
            # Skip disabled outputs and cryptomatte outputs
            if (out.enabled == False or 'Crypto' in out.identifier or 'Denois' in out.identifier or 'Noisy' in out.identifier):
                continue

            output_file_socket = output_node.file_slots.new(ComposeFullName(properties, out.identifier))
            render_socket = out
            
            if "Depth" == out.identifier:
                normalize_node = self.CreateNormalized((-200, 330))
                bpy.context.scene.node_tree.links.new(render_socket, normalize_node.inputs[0])        
                render_socket = normalize_node.outputs[0]    
            elif "Mist" == out.identifier:
                normalize_node = self.CreateNormalized((-200, 300))
                bpy.context.scene.node_tree.links.new(render_socket, normalize_node.inputs[0])        
                render_socket = normalize_node.outputs[0]
                
            if properties.denoise_all_passes and 'Image' != out.identifier: # Image is already denoised by Cycles
                denoise_node = self.CreateAndLinkDenoise(render_socket, normal_socket, albedo_socket, output_file_socket, (150, 375 - idx * 35))
            else:    
                bpy.context.scene.node_tree.links.new(render_socket, output_file_socket)
            idx += 1

    # --------------------- Output Node ------------------------
    def CreateFileOutput(self, position, properties):
        """Creates File Output node on given position and sets up its settings"""
        bpy.context.scene.render.image_settings.file_format = 'PNG'
        node = bpy.context.scene.node_tree.nodes.new("CompositorNodeOutputFile")
        node.label = 'Render Pass Output'
        node.base_path = properties.export_folder_path
        node.location = position
        node.width = 300
        node.use_custom_color = True
        node.color = (0.200, 0.200, 0.600)
        for input in node.inputs:
            node.inputs.remove(input)
        return node

    # ------------------- Render Other Layers -----------------------------
    def RenderOtherLayers(self, properties, main_layer_name):
        """Creates composition nodes for exporting other render layers"""
        view_layers = bpy.context.scene.view_layers
        layers = []
        for l in view_layers:
            if l.use:
                layers.append(l)
        if len(layers) < 2:
            return
        
        output_node = self.CreateFileOutput((500, 700), properties)
        
        for i in range(len(layers)):
            if layers[i].name == main_layer_name:
                continue
            new_rl = self.CreateRenderLayers((-500, 460 + i * 50), layers[i].name)
            new_rl.layer = layers[i].name
            new_rl.hide = True
            new_slot = output_node.file_slots.new(ComposeFullName(properties, layers[i].name))
            bpy.context.scene.node_tree.links.new(new_rl.outputs[0], new_slot)
    
    # ------------------- Execute -----------------------------
    def execute(self, context):
        """Render Pass Saver button execution function"""
        properties = bpy.context.scene.RenderPassSaverProperties
        
        if not bpy.context.scene.use_nodes:
            bpy.context.scene.use_nodes = True
            ClearAllNodes()
        
        if not bpy.context.view_layer.use:
            self.report({'ERROR'}, 'Current layer\'s \'Use for Rendering\' is not enabled in \'View Layer Properties\'!')
            ClearAllNodes()
            return {'CANCELLED'}    
        
        render_layer = bpy.context.view_layer.name
        layers_node = self.CreateRenderLayers((-500, 400), render_layer)
        output_node = self.CreateFileOutput((500, 450), properties)
        
        if properties.mask_by_collection and not self.IsCryptomatteObjectEnabled(layers_node):
            self.report({'ERROR'}, 'Cryptomatte Object pass is not enabled in \'View Layer Properties\'!')
            ClearAllNodes()
            return {'CANCELLED'}                    
        
        albedo_socket = self.FindAlbedoDenoiseSocket(layers_node)
        normal_socket = self.FindNormalDenoiseSocket(layers_node)
        if (properties.denoise_all_passes and not self.IsDenoiseEnabled(layers_node)) or \
            (properties.denoise_all_passes and (albedo_socket == None or normal_socket == None)):
            self.report({'ERROR'}, 'Denoise failed, please check \'Help\' for necessary settings.')
            ClearAllNodes()
            return {'CANCELLED'}

        self.LinkRenderPasses(layers_node, output_node, albedo_socket, normal_socket)        

        if properties.other_layers_combined:
            self.RenderOtherLayers(properties, render_layer)
                
        if properties.mask_by_collection:
            self.ApplyCryptomatteObjectMask(layers_node, output_node)

        # Render automaticaly
        if properties.auto_render:
            bpy.app.handlers.render_pre.append(self.PreRender)
            bpy.app.handlers.render_post.append(self.PostRender)
            bpy.app.handlers.render_cancel.append(self.CancelledRender)
            self.ui_timer = bpy.context.window_manager.event_timer_add(0.5, window = context.window)
            bpy.context.window_manager.modal_handler_add(self)
            return {"RUNNING_MODAL"}

        return {'FINISHED'}
    
        
    def modal(self, context, event):
        """Modal function handeling events"""
        if event.type != 'TIMER':
            return {"PASS_THROUGH"}
        
        # If render is cancelled or finished
        if self.render_done is True or self.render_stop is True:        
            # Remove the handlers and the modal timer
            bpy.app.handlers.render_pre.remove(self.PreRender)
            bpy.app.handlers.render_post.remove(self.PostRender)
            bpy.app.handlers.render_cancel.remove(self.CancelledRender)
            context.window_manager.event_timer_remove(self.ui_timer)
            # Open export folder
            properties = context.scene.RenderPassSaverProperties
            
            if self.render_stop is True:
                return {"CANCELLED"}
            
            if properties.open_export_folder:
                self.OpenExportFolder()
            return {"FINISHED"}
        
        if self.rendering is False and self.render_done is False:
            bpy.ops.render.render("INVOKE_DEFAULT", write_still=True)
        
        return {"PASS_THROUGH"}

# --------------------------------------------------------------

class ClearNodesButton(bpy.types.Operator):
    """Button for clearing all compositor nodes"""
    bl_idname = "node.clear_all_nodes"
    bl_label = "Clear All Nodes"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return bpy.context.scene.use_nodes and len(context.scene.node_tree.nodes) != 0

    def execute(self, context):
        ClearAllNodes()
        return {'FINISHED'}
    
# ----------------------------------------------------------------------

user_manual_text = [
    "ABOUT:",
    "",
    "    This add-on generates necessary Composite nodes for exporting various render passes automatically.",
    "    It can apply cryptomatte masks on user-defined collections and denoise rendered passes.",
    "    It can also render combined passes from other layers automaticaly - for rendering overbaked backgrounds.",
    "",
    "    Add-on was created for artists to speed up the process of export/import between Blender and Photoshop",
    "    It is designed for Cycles render engine, use of any other engines with this add-on is not recommended.",
    "",
    "    I Hope it helps, Michal Rudolf <michal.rudolf23@gmail.com>",
    "",
    "HOW TO USE IT:",
    "",
    "    BEFORE USING THE ADD-ON",
    "    Set up all properties before using the add-on. Add-on does not alter any settings in 'Properties' tab apart from Output, which is set up using Compositor nodes.",
    "    Organise your Scene Collection, so you have objects, that you want to mask out together, grouped in separate collections. Use names for these collections.",
    "    Root collection and collections containing only Lights and Cameras are skipped from cryptomate object masking.",
    "    Denoise in \'Render Properties\' have to be enabled for denoising. It's designed for blender's default denoiser (OpenImageDenoise - Albedo and Normal passes).",
    "    Set up other render layers and enable only those you want combined pass from.",
    "",
    "    ADD-ON SETTINGS",
    "    Export folder Path          - Select folder on local disk to stash all outputs.",
    "    Prefix                             - Choose something short and explainatory to help with file organization or leave it blank for no prefix.",
    "    Auto-render                   - Automatically starts the process of rendering after nodes are generated.",
    "    Open destination folder - Opends export folder after rendering is completed - for user to check exported render outputs.",
    "    Mask out by collection   - Create and export slices of the final image masked out by collection.",
    "    Denoise all passes         - Cycles only denoises Combined by default. If enabled, it denoises all passes after rendering.",
    "                                            Denoise effect will not be visible in the rendering window, but it will automatically apply on exported files.",
    "    Render other layers       - Render combined pass from other render layers enabled for rendering. Other layers can be used render overbaked backgrounds.",
    "    Hover the mouse over the setting option for description.",
    "",
    "    Once you finish setting up, push the 'Render Pass Saver' button.",
    "",
    "    Always clear Compositor before next generation with 'Clear All Nodes' button.",
    "",
    ]

def ShowUserManual():
    """Creates popup info dialog with user manual"""
    def draw(self, context):
        """Manuals draw function"""
        for line in user_manual_text:
            self.layout.label(text = line)

    bpy.context.window_manager.popup_menu(draw, title = "User Manual", icon = 'HELP')


class UserManualButton(bpy.types.Operator):
    """Show user manual"""
    bl_idname = "message.user_manual"
    bl_label = "Help"
  
    def execute(self, context):
        """Manual pop-up execution"""
        ShowUserManual()
        return {'FINISHED'} 



# ----------------------------------------------------------------------

class RenderPassSaver_PT_Panel(bpy.types.Panel):
    """Plugin's GUI panel located in Compositor tab > Sidebar"""
    bl_label = "Auto Render Pass Saver"
    bl_category = "Auto Rende Pass Saver"
    bl_space_type = "NODE_EDITOR"
    bl_region_type = "UI"

    def draw(self, context):
        """GUI Panels draw function"""
        layout = self.layout
        layout.operator(UserManualButton.bl_idname, icon = 'HELP')
        layout.label(text = "Options:")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "export_folder_path")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "output_prefix")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "auto_render")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "open_export_folder")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "mask_by_collection")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "denoise_all_passes")
        row = layout.row()
        layout.prop(context.scene.RenderPassSaverProperties, "other_layers_combined")
        row = layout.row()
        layout.label(text = "Clear:")
        row = layout.row()
        row.operator(ClearNodesButton.bl_idname, icon = 'TRASH')
        row = layout.row()
        layout.label(text = "Generate:")
        row = layout.row()
        row.scale_y = 2.0
        row.operator(RenderPassButton.bl_idname, icon = 'RENDER_RESULT')

# ----------------------------------------------------------------------

classes = (
    RenderPassSaver_PT_Panel,
    RenderPassButton,
    ClearNodesButton,
    UserManualButton,
    RenderPassSaverProperties
    )

def register():
    """Mandatory moduls register fuction"""
    for cls in classes:
        bpy.utils.register_class(cls)
    # Create custom scene property group pointer
    bpy.types.Scene.RenderPassSaverProperties = bpy.props.PointerProperty(type = RenderPassSaverProperties)

def unregister():
    """Mandatory moduls clean up functions"""
    for cls in classes:
        bpy.utils.unregister_class(cls)
    # Delete custom scene property group pointer
    del bpy.types.Scene.RenderPassSaverProperties

if __name__ == "__main__":
    register()
    