# Blender-Photoshop workflow plug-ins

These plug-ins/scripts were made to speed up the process of Bledner export and Photoshop import. They are made for artists that use various render passes generated from Blender.<br />
Blender add-on generates necessary compositor nodes for render pass export, it can create and apply cryptomatte object masks common for each collection, it can denoise all render passes, it can automatically start render process and render combined pass of all other enabled view layers.
<br />
Photoshop script imports all images from selected folder to the active document (or new document if none is open).<br />
<br />

If you combine cryptomatte masking and rendering combined pass from other layers: mask out forground objects and on another layer hide them, you can then in Photoshop easily and non-destructively move forground objects around and have their background rendered.
Cryptomatte masked render passes can replace clown pass.


## How to install
First of all, download this repositary (icon next to the 'Clone' button).
### Blender plug-in
Open Blender > Edit > Preferences... > Add-ons > Install... > Choose render_pass_saver.py from the downloaded repository > Enable it (Compositioning: Auto Render Pass Saver) in preferences<br /><br />

Add-on will appear in Compositor Tab > Sidebar > Auto Render Pass Saver.<br /><br />

### Photoshop script
Method 1: File > Scripts > Browse... > Select import_render_passes.jsx from the downloaded repository<br /><br />

Method 2: Locate Adobe Photoshop instalation folder on your local disk (Probably in 'Program Files > Adobe') > Presets > Scrips > Copy the import_render_passes.jsx to this folder > Restart Photoshop > File > Scripts > Now script is available the menu > Select folder to be imported<br /><br />

You can run the script from the document, where you want render passes to be imported or run it without any open documents (in that case it will create new document).

## How to use them
### Before using Blender add-on
Switch to your main view layer - add-on will be running on the active view layer.<br />
Set up all properties before using the add-on. Add-on does not alter any settings in 'Properties' tab apart from Output, which is set up using Compositor nodes.<br />
Organise your Scene Collection, so you have objects, that you want to mask out together, grouped in separate collections. Use names for these collections.<br />
Root collection and collections containing only Lights and Cameras are skipped from cryptomate object masking.<br />
Denoise in \'Render Properties\' have to be enabled for denoising. It's designed for blender's default denoiser (OpenImageDenoise - Albedo and Normal passes).<br />
Enable 'Use for rendering' on view layers you want combined pass from - This may extent the rendering time.

### Blender add-on
 ![Compositor nodes and add-on's UI](/imgs/nodes_and_gui.jpg)
All necessary information can be found under 'Help' button or you can hover the mouse over the setting option for description. <br />
<br />
Export folder Path      - Select folder on local disk to stash all outputs.<br />
Prefix                  - Choose something short and explainatory to help with file organization or leave it blank for no prefix.<br />
Auto-render             - Automatically starts the process of rendering after nodes are generated.<br />
Open destination folder - Opends export folder after rendering is completed - for user to check exported render outputs.<br />
Mask out by collection  - Create and export slices of the final image masked out by collection. You can use them instead of Clown pass.<br />
Denoise all passes      - Cycles only denoises Combined by default. If enabled, it denoises all passes after rendering.<br />
                          Denoise effect will not be visible in the rendering window, but it will automatically apply on exported files.<br />
Render other layers     - Render combined pass of every other render layer, that is enabled in View Layer Properties.<br />

### Photoshop script
The user interaction is simple, you only select the folder with exported files and it will import all supported files (PNG, JPG, GIF, TIF, BMP)


### Limitations
This add-on is designed for use with Cycles engine. Some features may not work properly with other render engines.
Denoise is designed for blender's default denoiser (OpenImageDenoise - Albedo and Normal passes).
Add-on does not care about any user-places Compositor nodes.
All outputs are exported in PNG.

=============================================================================================

If you are willing to try them out, please reach me on artstation or on this emain <michal.rudolf23@gmail.com>.

Valued feedback about the plug-ins:
+ Was it difficult to figure out?
+ Does it align with your workflow?
+ Do you find it time-saving?
+ Are exported passes just as you rendered them before?
+ Do you think, that cryptomatte masks are better solution than random flat workbench render?
+ Does it enable features, that were not available to you before?

Thanks,
Michal